<?php

namespace Drupal\slugify;

use Cocur\Slugify\Slugify;

/**
 * Class SlugifyText.
 *
 * @package Drupal\slugify
 */
class SlugifyText {
  public function slugifytext($str, $operator) {
    $new_slugify = new Slugify();
    $new_text = $new_slugify->slugify($str, $operator);
    return $new_text;
  }
}
