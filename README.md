Slugify a Text Field, Drupal 8 module
=====================================

Installation with Composer
==========================

on <drupal-root> directory, in the command line run in order the following 3 commands:

	git -C modules clone https://bitbucket.org/panchocadena/slugify


    composer require drupal/libraries
  

    composer require cocur/slugify
  

enable module

on text field display & cog wheel settings set configuration.